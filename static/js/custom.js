$(document).ready(function(){
	get_rss_info("http://feeds.feedburner.com/hispasec/zCAd", "uad");
	get_rss_info("http://feeds.feedburner.com/hispasec/IcLa", "lab");

	//Contact - scroll to map
	$(function(){
		$('.map-show').on('click', scrollToDown);
	});

	function scrollToDown() {
		$('html, body').animate({scrollTop: $("#mapSection").offset().top},800);
	}
});

function checkCookieLaw()
{
    function setCookie(c_name,value,exdays)
    {
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    }
    function getCookie(c_name)
    {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
        {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1)
        {
            c_value = null;
        }
        else
        {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1)
            {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    }

    var cookieName = "cookies-law";
    var domAlert = "#cookies-law";

    if(!getCookie(cookieName))
    {
        //Show cookie law alert & set the cookie
        $(domAlert).show();
        setCookie(cookieName, 1, 365);
    }
    else
    {
        //Delete cookie law alert form dom
        $(domAlert).remove();
    }
}